from django.db import models

# Create your models here.


class VetsType(models.Model):
    vets_type_name_ru = models.CharField(max_length=256)
    vets_type_name_kz = models.CharField(max_length=256)

    def __str__(self):
        return self.vets_type_name_ru


class Manufacturer(models.Model):
    manufacturer_name_ru = models.CharField(max_length=512)
    manufacturer_name_kz = models.CharField(max_length=512)

    def __str__(self):
        return self.manufacturer_name_ru


class Role(models.Model):
    role_name = models.CharField(max_length=128)

    def __str__(self):
        return self.role_name


class User(models.Model):
    login_name = models.CharField(max_length=50)
    email = models.CharField(max_length=256)
    password = models.CharField(max_length=128)
    full_name = models.CharField(max_length=256)
    roles = models.ManyToManyField('Role', through='UserRole', through_fields=('user', 'role'))

    def __str__(self):
        return self.full_name


class UserRole(models.Model):
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str.join(self.user.full_name, '-', self.role.role_name)


class Region(models.Model):
    region_name = models.CharField(max_length=255)

    def __str__(self):
        return self.region_name


class VetsApplication(models.Model):
    firstname = models.CharField(max_length=256)
    lastname = models.CharField(max_length=256)
    middlename = models.CharField(max_length=256)
    iin = models.CharField(max_length=12)
    bin = models.CharField(max_length=12)
    document_number = models.CharField(max_length=50)
    vets_type = models.ForeignKey(VetsType, on_delete=models.CASCADE)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    model = models.CharField(max_length=256)
    registration_number = models.CharField(max_length=128)
    application_date = models.DateTimeField(null=False, auto_now=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    def __str__(self):
        return self.registration_number


class VetsPhoto(models.Model):
    vets_application = models.ForeignKey(VetsApplication, on_delete=models.CASCADE)
    file = models.ImageField(upload_to='photos/%Y/%m/%d')

    def __str__(self):
        return self.vets_application.registration_number

