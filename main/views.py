from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from .models import VetsApplication, VetsPhoto
from .forms import ActForm, VetsPhotoForm
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import render, get_object_or_404, get_list_or_404


# Create your views here.
@login_required
def index(request):
    return HttpResponse('3214')


def act_details(request, act_id):
    return HttpResponse('12')


class VetsPhotoEmpty(LoginRequiredMixin, CreateView):
    def get(self, request):
        file_form = VetsPhotoForm(instance=VetsPhoto())
        context = {'form': file_form}
        template = 'main/photo/empty_form.html'
        return render(request, template, context)


class ActsListView(LoginRequiredMixin, ListView):
    model = VetsApplication
    paginate_by = 20
    template_name = 'main/vets/vetsapplication_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now
        return context


class ActUpdateView(LoginRequiredMixin, UpdateView):
    model = VetsApplication
    template_name_suffix = '_create'
    form_class = ActForm

    def get(self, request, act_id=None):
        template = 'main/vets/vetsapplication_create.html'
        if act_id:
            act = get_object_or_404(VetsApplication, id=act_id)
            act_form = ActForm(instance=act)

            vets_photo_list = VetsPhoto.objects.filter(vets_application_id=act_id)
            if vets_photo_list.count() > 0:
                vets_photo_forms = []
                for photo in vets_photo_list:
                    vets_photo_forms.append(VetsPhotoForm(instance=photo))
            else:
                vets_photo_forms = {VetsPhotoForm(instance=VetsPhoto())}

        else:
            act_form = ActForm(instance=VetsApplication())
            vets_photo_forms = {VetsPhotoForm(instance=VetsPhoto())}
        context = {'form': act_form, 'photo_forms': vets_photo_forms}
        return render(request, template, context)

    def post(self, request, act_id=None):
        act_form = ActForm(request.POST)

        files = request.FILES.getlist('file')
        if act_form.is_valid():
            new_form = act_form.save(commit=False)
            new_form.save()
            for f in files:
                photo = VetsPhoto(file=f, vets_application=new_form)
                photo.save()

            return HttpResponseRedirect(reverse('main:acts-list'))


class ActCreateView(LoginRequiredMixin, CreateView):
    model = VetsApplication
    template_name_suffix = '_create'
    form_class = ActForm

    def get(self, request, act_id=None):
        template = 'main/vets/vetsapplication_create.html'
        if act_id:
            act = get_object_or_404(VetsApplication, id=act_id)
            act_form = ActForm(instance=act)

            vets_photo_list = VetsPhoto.objects.filter(vets_application_id=act_id)
            if vets_photo_list.count() > 0:
                vets_photo_forms = []
                for photo in vets_photo_list:
                    vets_photo_forms.append(VetsPhotoForm(instance=photo))
            else:
                vets_photo_forms = {VetsPhotoForm(instance=VetsPhoto())}

        context = {'form': act_form, 'photo_forms': vets_photo_forms}
        return render(request, template, context)

    def post(self, request, act_id=None):
        context = {}
        if act_id is not None:
            instance = VetsApplication.objects.get(pk=act_id)

        act_form = ActForm(request.POST, instance=instance)

        if act_form.is_valid():
            new_form = act_form.save(commit=False)
            new_form.save()

            form = VetsPhotoForm(request.POST, request.FILES)

            newform=VetsPhoto(file=form.files, vets_application=new_form)
            if form.is_valid():
                newform.save()

            return HttpResponseRedirect(reverse('main:acts-list'))

    def get_success_url(self):
        return reverse('main:act-details', self.model.id)

