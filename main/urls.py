from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path
from main import views
from .views import ActsListView, ActCreateView, ActUpdateView, VetsPhotoEmpty

app_name = "main"
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='main/account/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='main/account/logout.html'), name='logout'),
    url(r'^password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('', views.index, name='index'),

    url(r'^acts/$', ActsListView.as_view(), name='acts-list'),
    url(r'^acts/create/$', ActCreateView.as_view(), name='act-create'),
    path('acts/update/<int:act_id>/', ActUpdateView.as_view(), name='act-edit'),
    path('acts/details/<int:act_id>/', views.act_details, name='act-details'),
    path('acts/photo/empty/', VetsPhotoEmpty.as_view(), name='vets-photo-empty'),
]
