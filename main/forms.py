from django.forms.models import ModelForm
from django.forms import Select, TextInput, FileInput
from django.utils.translation import gettext_lazy as _
from .models import VetsApplication, VetsPhoto
from django import forms


class VetsPhotoForm(ModelForm):
    class Meta:
        model = VetsPhoto
        fields = '__all__'
        exclude = {'vets_application'}
        widgets = {'file': forms.ClearableFileInput(attrs={'multiple': True})}
        labels = {'file': _('file')}


class ActForm(ModelForm):
    class Meta:
        model = VetsApplication
        fields = '__all__'
        labels = {
            'firstname': _('firstname'),
            'lastname': _('lastname'),
            'middlename': _('middlename'),
            'vets_type': _('application_vets_type'),
            'region': _('region'),
            'iin': _('iin'),
            'bin': _('bin'),
            'document_number': _('document_number'),
            'registration_number': _('registration_number'),
            'manufacturer': _('manufacturer'),
            'model': _('model'),
        }

        widgets = {
            'vets_type': Select(attrs={'class': 'form-control'}),
            'region': Select(attrs={'class': 'form-control'}),
            'manufacturer': Select(attrs={'class': 'form-control'}),
            'firstname': TextInput(attrs={'class': 'form-control'}),
            'lastname': TextInput(attrs={'class': 'form-control'}),
            'middlename': TextInput(attrs={'class': 'form-control'}),
            'iin': TextInput(attrs={'class': 'form-control'}),
            'bin': TextInput(attrs={'class': 'form-control'}),
            'document_number': TextInput(attrs={'class': 'form-control'}),
            'registration_number': TextInput(attrs={'class': 'form-control'}),
            'model': TextInput(attrs={'class': 'form-control'}),
        }
