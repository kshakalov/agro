from django.contrib import admin
from .models import User, Role, UserRole, VetsPhoto, VetsType, Manufacturer, VetsApplication, Region
# Register your models here.
admin.site.register(User)
admin.site.register(Role)
admin.site.register(UserRole)
admin.site.register(Manufacturer)
admin.site.register(VetsType)
admin.site.register(Region)
