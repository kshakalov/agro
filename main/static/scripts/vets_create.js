$(document).ready(function(){
    addPhotoField();
});


function addPhotoField(){
    $.get(emptyFormUrl, function (res){
        let photoCount=0;
        $("#photosDiv").append(res);
        $('#photosDiv').find('[id*=file]').each(function(){
            photoCount += 1;
            $(this).attr("id", `id_file_${photoCount}`);
        });
    });
}